﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmiiboQuiz : MonoBehaviour
{

	public List<Amiibo> amiibos = new List<Amiibo>();
	private int counter = 0;
	public Image amiiboImageHolder;
	private Amiibo currentAmiibo;
	public Text[] answers = new Text[3];
	public Text scoreBox;
	public int score = 0;
	public Button endGame;

	void Start()
	{
		if (LoadAmiibos.Instance == null)  //if this object does not exist, add and load the data
		{
			this.gameObject.AddComponent<LoadAmiibos>();
			LoadAmiibos.Instance.loadXML();
		}

		amiibos = LoadAmiibos.Instance.amiibos;
		selectNextAmiibo(0);
	}



	public void selectNextAmiibo(int selected)
	{
		if (counter > 0) checkAnswer(selected);
		currentAmiibo = amiibos[Random.Range(0, amiibos.Count)];

		setCanvasElements();

		counter++;

		if (counter == 26)
		{
			endQuiz();
		}
	}

	private void checkAnswer(int selected)
	{
		if (answers[selected].text.CompareTo(currentAmiibo.getFullName()) == 0)
		{
			score++;
		}

	}

	private void setCanvasElements()
	{
		addRandomNamesToAnswerButtons();
		amiiboImageHolder.sprite = Sprite.Create(currentAmiibo.getAmiiboTexture(), new Rect(0, 0,
			currentAmiibo.getAmiiboTexture().width, currentAmiibo.getAmiiboTexture().height), new Vector2(.5f, .5f));
		int correctAnswer = Random.Range(0, 3);
		answers[correctAnswer].text = currentAmiibo.getFullName();
		scoreBox.text = "Score " + score + "/" + counter + "\n (Out of 25)";


	}

	private void addRandomNamesToAnswerButtons()
	{
		foreach (Text answer in answers)
		{
			answer.text = amiibos[Random.Range(0, amiibos.Count)].getFullName();
		}
	}

	private void endQuiz()
	{
		foreach (Text answer in answers)
		{
			answer.GetComponentInParent<Button>().enabled = false;
		}

		endGame.gameObject.SetActive(true);
		
	}

}
