﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoadAmiibos : Singleton<LoadAmiibos>
{
	public List<Amiibo> amiibos = new List<Amiibo>();
	public Amiibo currentAmiibo { get; set;}

	public override void Awake()
	{
		base.Awake();
		if (LoadAmiibos.Instance.amiibos.Count == 0) loadXML();
	}

	public void loadXML()
	{
		TextAsset textData = Resources.Load<TextAsset>("Amiibos");
		parseAmiibosXML(textData.text);		
	}

	private void parseAmiibosXML(string xmlData)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(xmlData));

		string xmlPathPattern = "//amiibos/amiibo";
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);
		
		foreach (XmlNode node in myNodeList)
		{
			Amiibo amiibo = new Amiibo(int.Parse(node.Attributes["identifier"].Value), node.Attributes["image"].Value, node.InnerText);
			amiibos.Add(amiibo);
		}
	}
}